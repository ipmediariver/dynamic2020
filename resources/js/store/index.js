import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({

	state: {
		auth: false,
		user: null,
	},

	mutations: {
		set_auth(state, data){
			state.auth = data;
		},
		set_user(state, data){
			state.user = data;
		}
	},

	getters: {
		get_auth(state){
			return state.auth;
		},
		get_user(state){
			return state.user;
		}
	},

	actions: {
		get_auth_user(context){
			let t = context;
			axios.get('/api/user').then(({data}) => {
				t.commit('set_auth', true);
				t.commit('set_user', data);
			}).catch((error) => {
				t.commit('set_auth', false);
				t.commit('set_user', null);
			});
		}
	}

});

export default store;