import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

// Auth views
import Login from '../views/auth/Login'; 
import SignUp from '../views/auth/SignUp';

// Website views
import PageNotFound from '../views/PageNotFound';
import Home from '../views/Home';

const router = new VueRouter({

	mode: 'history',
	routes: [
		{
			path: '/',
			component: Home,
			name: 'home'
		},

		{
			path: '/*',
			component: PageNotFound
		},

		{
			path: '/login',
			component: Login,
			name: 'login'
		},

		{
			path: '/signup',
			component: SignUp,
			name: 'signup'
		},
	],

});

export default router;

